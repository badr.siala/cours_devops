# Cours DevOps



## Cours

[Chapitre 1](Cours/devops_chap1.pdf)

[Chapitre 2](Cours/devops_chap2.pdf)

[Chapitre 3](Cours/devops_chap3.pdf)

[Chapitre 4](Cours/devops_chap4.pdf)

[Chapitre 5](Cours/devops_chap5.pdf)

## Travaux Pratiques

[TP 1](Travaux_Pratiques/tp1_debug_c_devops2.pdf)

[TP 2](Travaux_Pratiques/tp_git_devops_commandes.pdf)

[TP 3](Travaux_Pratiques/tp_git_devops_commandes.pdf)
